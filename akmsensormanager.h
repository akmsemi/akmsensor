#ifndef AKMSENSORMANAGER_H
#define AKMSENSORMANAGER_H

#include "akdphwinfo.h"
#include "mbed.h"
#include "ble/BLE.h"
#include "ble/services/UARTService.h"
#include "SerialNano.h"
#include "Message.h"
#include "mcp342x.h"
#include "akmsensor.h"
#include "akmakd.h"
#include "akmhallswitch.h"
#include "akmanalogsensor.h"
#include "ak09970ctrl.h"
#include "ak9750ctrl.h"
#include "ak9752ctrl.h"
#include "ak9754ctrl.h"
#include "ak9756ctrl.h"
#include "ak7451ctrl.h"
#include "ak7452ctrl.h"
#include "ak7401ctrl.h"
#include "ap1017ctrl.h"

#define AKMSENSORMANAGER_MAGNETOMETER_ID			0x0A
#define AKMSENSORMANAGER_MAX_SENSOR_NUM				2
//#define AKMSENSORMANAGER_LEN_NAME_MAX				20

/**
 * Unified manager class for all sensors to interact with AkmSensor.
 */
class AkmSensorManager {

public:
   
    /** 
     * Return status enumeration for debugging.
     */
    typedef enum {
        SUCCESS = 0,    /**< Successful execution (0) */
        ERROR,          /**< Error in execution (1) */
    } Status;
    
    /**
     * Class constructor with an initial serial connection.
     */
    AkmSensorManager(SerialNano* com);
    
    /** 
     * Initialize sensor manager.
     * 
     * @param id Primary ID of daughter board device.
     * @param subID Secondary ID of daughter board device.
     * @return Status type: SUCCESS=0.
     */
    Status init(uint8_t id, uint8_t subid);
    
    /**
     * Set BLE UART service.
     * 
     * @param service UART service type
     */
    void setBleUartService(UARTService* service);
    
    /**
     * Sets eventConnected flag to TRUE.
     */
    void setEventConnected();
    
    /** 
     * Sets eventDisconnected flag to TRUE.
     */
    void setEventDisconnected();
    
    /** 
     * Sets eventCommandReceived flag to TRUE.
     * 
     * @param buf Command to be parsed and to check the validity.
     * @return Status type: SUCCESS=0
     */
    Status commandReceived(char* buf);
    
    /**
     * Checks if an event has occurred.
     * 
     * @return Returns SUCCESS(=0) if a sensor event has occurred, or if any of the 
     *         eventCommandReceived, eventConnected, or eventDisconnected flags
     *         are set. Returns ERROR otherwise.
     */
    bool isEvent();
    
    /**
     * Main function that processes input, output, and connection events.
     * 
     * @return Status type: SUCCESS=0.
     */
    Status processEvent();
    
    /**
     * Get the name of the sensor as a string.
     *
     * @return Returns the address of the string containing the name.
     */
    char* getSensorName();
    
    /**
     * Processes the stored command, parses the arguments, then uses the 
     * command to throw a message.
     */
    void processCommand();
    
    /**
     * Processes command and arguments in message from char to ASCII and 
     * sends to BLE and/or USB serial device(s).
     *
     * @param msg Message containing command to be processed.
     * @return Status type: SUCCESS=0
     */
    Status throwMessage(const Message *msg);
    
//    void releaseTWI();

    /**
     * Get primary or secondary ID from sensor device.
     *
     * @param pin Pin number of ID or SubID signal.
     * @param bits Number of bits of precision of value.
     * @return Returns the integer ID or SubID value.
     */
    static uint8_t getId(PinName pin, uint8_t bits);
    
    /**
     * Detect if data is ready to be read.
     */
    void detectDRDY();
        
private:
    
    AkmSensor*		sensor[AKMSENSORMANAGER_MAX_SENSOR_NUM];
    SerialNano*		serial;
    UARTService*	uartService;
    Message msg;
    Timeout t;

    InterruptIn*    interrupt;

    uint8_t         primaryId;
    uint8_t         subId;
    uint8_t         sensorIndex;
    uint8_t         sensorNum;
    bool			isEnabledBle;
    bool			isEnabledUsb;

    bool			eventCommandReceived;
    bool			eventConnected;
    bool			eventDisconnected;
    char			userCommand[20];

    AkmAkd::InterruptMode drdyType;
    
    bool checkAkmSensor();
    void dataOut(char* str);
    void commandReceivedCallback();
    char* stringConcat(char* str1, char* str2);
    void attachInterrupt();
    void detachInterrupt();
#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    static int16_t getAdcData(MCP342X *mcp3428, MCP342X::AdcChannel ch, MCP342X::SampleSetting s);
#endif
};

#endif // AKMSENSORMANAGER_H
