#ifndef AK9750CTRL_H
#define AK9750CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "AK9750.h"

/**
 * Class for handling commands issued to the AK9750/AK9753.
 */
class Ak9750Ctrl : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /**
     * Device Sub-IDs.
     */
    typedef enum {
        SUB_ID_AK9750              = 0x01,      /**< AK9750: ID = 01h */
        SUB_ID_AK9753              = 0x03,      /**< AK9753: ID = 03h */
    } SubIdIrSensor;

    /**
     * Constructor.
     *
     */
    Ak9750Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak9750Ctrl();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);

    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status requestCommand(Message* in, Message* out);

    /**
     * Set the event flag.
     */
    virtual void setEvent();
    
private:
    AK9750*         ak9750;
    
    // hold settings
    AK9750::OperationMode mode;
    AK9750::DigitalFilter filter;
};

#endif
