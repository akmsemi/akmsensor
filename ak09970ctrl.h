#ifndef AK09970CTRL_H
#define AK09970CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "ak09970.h"
#include "ak09970_reg.h"

/**
 * Class for handling commands issued to the AK09970.
 */
class Ak09970Ctrl : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /** 
     * Device Sub-ID (5-bit ID).
     */
    typedef enum {
        SUB_ID_AK09970N				= 0x03,	/**< AK09970N: ID = 03h */
		SUB_ID_AK09970D				= 0x04,	/**< AK09970D: ID = 04h */
    } SubIdSwitch;
    
    /**
     * Constructor.
     *
     */
    Ak09970Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak09970Ctrl();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status requestCommand(Message* in, Message* out);
       
    /**
     * Set event flag.
     */
    virtual void setEvent();
    
private:
    AK09970*        ak09970;

    // hold settings for AK09970
    AK09970::Threshold threshold;
    AK09970::OperationMode mode;
    AK09970::SensorDriveMode sensorDriveMode;
    AK09970::SensorMeasurementRange sensorMeasurementRange;
    AK09970::ReadConfig readConfig;
    AK09970::SwitchConfig switchConfig;
};

#endif

