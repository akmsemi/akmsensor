#ifndef AK9754CTRL_H
#define AK9754CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "AK9754.h"

#define CMD_IR_GET_OPERATION_SETTINGS	0x92
#define CMD_IR_SET_OPERATION_SETTINGS	0x93
#define CMD_IR_GET_HBD_SETTINGS			0x98
#define CMD_IR_SET_HBD_SETTINGS			0x99

/**
 * Class for handling commands issued to the AK9754.
 */
class Ak9754Ctrl : public AkmSensor
{

public:
    
    /**
     * Device Sub-ID
     */
    typedef enum {
        SUB_ID_AK9754              = 0x04       /**< AK9754: 04h */
    } SubIdAk9754;

    /**
     * Constructor.
     *
     */
    Ak9754Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak9754Ctrl();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);

    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status requestCommand(Message* in, Message* out);
    
    /**
     * Set event flag.
     */
    virtual void setEvent();

private:
    AK9754*         		ak9754;
    
    AK9754::SensorData		data;
    AK9754::OperationMode	mode;
    AK9754::OpSettings		settings;
    AK9754::HbdSettings		hbdSettings;
    AK9754::Threshold		threshold;
	AK9754::InterruptEnable interruptEnable;
};

#endif
