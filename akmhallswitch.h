#ifndef AKMHALLSWITCH_H
#define AKMHALLSWITCH_H

#include "mbed.h"
#include "akmsensor.h"

/**
 * Collection class for handling commands to all AKM Hall Switch modules.
 * Hall Swich Devices:
 *
 *      Unipolar Switches: EM1771, EW453, EW652B, EW6672
 *
 *      Omnipolar Switches: EM1781, AK8788A, EM6781
 *
 *      Bipolar Latches: AK8771, EZ470, EZ471, EW432, EW612B, EW632
 *      
 *      Dual Switches: AK8789, EM1791
 *
 *      One-Chip Encoders: AK8775, AK8779A, AK8779B, AK8776
 */
class AkmHallSwitch : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /** 
     * Unipolar Switch Sub-IDs
     *
     * Primary ID = 0x01
     */
    typedef enum {
        SUB_ID_EM1771               = 0x01,     /**< EM1771 = 0x01 */
        SUB_ID_EW453                = 0x03,     /**< EW453 = 0x03 */
        SUB_ID_EW652B               = 0x0C,     /**< EW652B = 0x0C */
        SUB_ID_EW6672               = 0x0D,     /**< EW6672 = 0x0D */
    } SubIdUnipolarSwitch;                 

    /**
     * Omnipolar Switch Sub-IDs
     *
     * Primary ID = 0x02
     */
    typedef enum {
        SUB_ID_EM1781               = 0x03,     /**< EM1781 = 0x03 */
        SUB_ID_AK8788A              = 0x04,     /**< AK8788A = 0x04 */
        SUB_ID_EM6781               = 0x05,     /**< EM6781 = 0x05 */
    } SubIdOmnipolarSwitch;

    /**
     * Bipolar Latch Sub-IDs
     *
     * Primary ID = 0x03
     */
    typedef enum {
        SUB_ID_AK8771               = 0x01,     /**< AK8771 = 0x01 */
        SUB_ID_EZ470                = 0x03,     /**< EZ470 = 0x03 */
        SUB_ID_EZ471                = 0x04,     /**< EZ471 = 0x04 */
        SUB_ID_EW432                = 0x08,     /**< EW432 = 0x08 */
        SUB_ID_EW612B               = 0x0C,     /**< EW612B = 0x0C */
        SUB_ID_EW632                = 0x0D,     /**< EW632 = 0x0D */
    } SubIdBipolarLatch;

    /**
     * Dual Output Switch Sub-IDs
     *
     * Primary ID = 0x04
     */
    typedef enum {
        SUB_ID_AK8789               = 0x03,     /**< AK8789 = 0x03 */
        SUB_ID_EM1791               = 0x04,     /**< EM1791 = 0x04 */
    } SubIdDualSwitch;

    /**
     * One-Chip Encoder Sub-IDs
     *
     * Primary ID = 0x05
     */
    typedef enum {
        SUB_ID_AK8775               = 0x01,     /**< AK8775 = 0x01 */
        SUB_ID_AK8779A              = 0x0B,     /**< AK8779A = 0x0B */
        SUB_ID_AK8779B              = 0x0C,     /**< AK8779B = 0x0C */
        SUB_ID_AK8776               = 0x0F,     /**< AK8776 = 0x0F */
    } SubIdOnechipEncoder;

    AkmHallSwitch();
    virtual ~AkmHallSwitch();

    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);

    /**
     * Callback function for a rising edge event on pin D0
     */
    void riseEventD0();
    
    /**
     * Callback function for a falling edge event on pin D0
     */
    void fallEventD0();
    
    /**
     * Callback function for a rising edge event on pin D1
     */
    void riseEventD1();
    
    /**
     * Callback function for a falling edge event on pin D1
     */
    void fallEventD1();

private: 
    uint8_t         d0;
    uint8_t         d1;
    InterruptIn*    sw0;
    InterruptIn*    sw1;
};

#endif
