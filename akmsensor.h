#ifndef AKMSENSOR_H
#define AKMSENSOR_H

#include "akdphwinfo.h"
#include "mbed.h"
#include "Message.h"
#include "Debug.h"


/**
 * Abstract base class for general AKM sensors.
 */
class AkmSensor
{

public:
    /**
     * Error type for debugging purposes.
     */
    typedef enum {

        SUCCESS = 0,            /**< Successful termination (0) */
        ERROR,                  /**< Error during execution (1) */
    } Status;
    
    /**
     * Primary IDs for the major categories of sensors.
     */
    typedef enum {
        AKM_PRIMARY_ID_AKD_SPI              = 0x0,      /**< AKD SPI Devices */
        AKM_PRIMARY_ID_UNIPOLAR             = 0x1,      /**< Unipolar Switches */
        AKM_PRIMARY_ID_OMNIPOLAR            = 0x2,      /**< Omnipolar Switches */
        AKM_PRIMARY_ID_LATCH                = 0x3,      /**< Bipolar Latches */
        AKM_PRIMARY_ID_DUAL_OUTPUT          = 0x4,      /**< Dual Output Switches */
        AKM_PRIMARY_ID_ONECHIP_ENCODER      = 0x5,      /**< One-Chip Encoders */
        AKM_PRIMARY_ID_TBD1                 = 0x6,      /**< Undefined */
        AKM_PRIMARY_ID_CURRENT_SENSOR_3V    = 0x7,      /**< Current Sensor 3V Output */
        AKM_PRIMARY_ID_DEMO                 = 0x8,      /**< Demo Sensors */
        AKM_PRIMARY_ID_CURRENT_SENSOR_5V    = 0x9,      /**< Current Sensors 5V Output */
        AKM_PRIMARY_ID_MISC_ANALOG          = 0xA,      /**< Analog Devices */
        AKM_PRIMARY_ID_LINEAR_SENSOR        = 0xB,      /**< Linear Sensors */
        AKM_PRIMARY_ID_MOTOR_DRIVER         = 0xC,      /**< Motor Drivers */
        AKM_PRIMARY_ID_IR_SENSOR            = 0xD,      /**< IR Sensors */
        AKM_PRIMARY_ID_ANGLE_SENSOR         = 0xE,      /**< Angle Sensors */
        AKM_PRIMARY_ID_AKD_I2C              = 0xF,      /**< AKD I2C Devices */
    } SensorPrimaryId;


    virtual ~AkmSensor(){};

    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual Status init(const uint8_t id, const uint8_t subid) = 0;
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual Status startSensor() = 0;
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual Status startSensor(const float sec) = 0;
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual Status stopSensor() = 0;
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual Status readSensorData(Message* msg) = 0;
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out) = 0;
    
    /**
     * Set event flag.
     */
    virtual void setEvent(){
        DBG_L4("#AKM Sensor - setEvent called.\r\n");
        event = true;
    }
    
    /**
     * Clear event flag.
     */
    void clearEvent(){
        DBG_L4("#AKM Sensor - clearEvent called.\r\n");
        event = false;
    }
    
    /**
     * Checks if an event has occurred.
     *
     * @return TRUE if event has occurred, FALSE otherwise.
     */
    bool isEvent(){
        return event;
    }
    
    /**
     * Retrieve the name of the sensor.
     *
     * @return Name of sensor as a character array.
     */
    const char* getSensorName(){
        return sensorName;
    };
    
    /**
     * Retrieve the primary ID of the sensor.
     *
     * @return Primary ID as an integer.
     */
    int getPrimaryId(){
        return primaryId;
    };
    
    /**
     * Retrieve the Sub-ID of the sensor.
     *
     * @return Sub-ID as an integer.
     */
    int getSecondaryId(){
        return subId;
    };

private:
    bool            event;

protected:
    uint8_t         primaryId;
    uint8_t         subId;
    const char*     sensorName;
    
    AkmSensor(){
        event = false;
        primaryId = 0;
        subId = 0;
        sensorName = "";
    };
};

#endif
