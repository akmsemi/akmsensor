#include "Ak9754Ctrl.h"
#include "AK9754_reg.h"
#include "AK9754.h"

#define CONV16I(high,low)  ((int16_t)(((high) << 8) | (low)))


Ak9754Ctrl::Ak9754Ctrl() : AkmSensor(){
    ak9754 = NULL;
    // TODO: Initialize all AK9754 settings here
}

Ak9754Ctrl::~Ak9754Ctrl(){
    if (ak9754) delete ak9754;
}

AkmSensor::Status Ak9754Ctrl::init(const uint8_t id, const uint8_t subid){

	primaryId = id;
    subId = subid;
    bool foundSensor = false;

    AK9754::SlaveAddress slaveAddr[] = { AK9754::SLAVE_ADDR_1 };

    I2C* i2c = new I2C(I2C_SDA,I2C_SCL);
    i2c->frequency(I2C_SPEED);

    if(subId == SUB_ID_AK9754){
		ak9754 = new AK9754();
		sensorName = "AK9754";
    }
    else{
    	DBG_L1("#AK9754 Control - Error: Invalid Sub-ID.\r\n");
        return AkmSensor::ERROR;
    }

    for(unsigned int i=0; i<sizeof(slaveAddr); i++)
    {
        ak9754->init(i2c, slaveAddr[i]);

        if(ak9754->verifyDevice() == AK9754::SUCCESS) {
            foundSensor = true;
            break;
        }
    }

    if(foundSensor != true){
    	DBG_L1("#AK9754 Control - Error: No sensor found.\r\n");
    	return AkmSensor::ERROR;
    }

    // reset
    if (ak9754->reset() != AK9754::SUCCESS) {
        DBG_L1("#AK9754 Control - Error: Failed to reset AK9754.\r\n");
    }
        
    DBG_L3("#AK9754 Control - Initialization success.\r\n");
    return AkmSensor::SUCCESS;
}

void Ak9754Ctrl::setEvent(){
    DBG_L3("#AK9754 Control - setEvent().\r\n");

    AK9754::Status status;

    status = ak9754->isDataReady();

    if(status == AK9754::DATA_READY)
    	AkmSensor::setEvent();
}

AkmSensor::Status Ak9754Ctrl::startSensor(){

	mode = AK9754::MODE_CONTINUOUS;

    // Read data once to clear interrupt
	ak9754->getSensorData(&data);

	// Initialize operational and HBD settings
	ak9754->setOperationSettings(&settings);
	ak9754->setHbdSettings(&hbdSettings);
    
    if(ak9754->setOperationMode(&mode) != AK9754::SUCCESS) {
        DBG_L1("#AK9754 Control - Error: Start sensor failed %s\r\n", sensorName);
        return AkmSensor::ERROR;
    }

    DBG_L3("#AK9754 Control - Sensor started\r\n");
    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9754Ctrl::startSensor(const float sec){
    return AkmSensor::ERROR;
}

AkmSensor::Status Ak9754Ctrl::stopSensor(){

	mode = AK9754::MODE_STANDBY;

	AkmSensor::clearEvent();

    if(ak9754->setOperationMode(&mode) != AK9754::SUCCESS) {
        DBG_L1("#AK9754 Control - Error: Unable to set operation mode.\r\n");
        return AkmSensor::ERROR;
    }
    
    // Read data once to clear interrupt
    ak9754->getSensorData(&data);
    
    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9754Ctrl::readSensorData(Message* msg){

    AkmSensor::clearEvent();

    if(ak9754->getSensorData(&data) != AK9754::SUCCESS){
        DBG_L1("#AK9754 Control - Error: Unable to get sensor data.\r\n");
        return AkmSensor::ERROR;
    }

    msg->setCommand(Message::CMD_START_MEASUREMENT);
    msg->setArgument(0, (char)data.intStatus.drdy);
    msg->setArgument(1, (char)data.intStatus.hbdr1);
    msg->setArgument(2, (char)data.intStatus.hbdr2);
    msg->setArgument(3, (char)((int32_t)(data.ir) >> 8));
    msg->setArgument(4, (char)((int32_t)(data.ir) & 0x00FF) );
    msg->setArgument(5, (char)((int32_t)(data.temperature) >> 8));
    msg->setArgument(6, (char)((int32_t)(data.temperature) & 0x00FF) );
    msg->setArgument(7, (char)data.dor);

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9754Ctrl::requestCommand(Message* in, Message* out){

	AkmSensor::Status status = AkmSensor::SUCCESS;
    Message::Command cmd;

    cmd = in->getCommand();
    
    out->setCommand(cmd);
    
    switch(cmd){
        case Message::CMD_IR_GET_THRESHOLD:
        {
            if (ak9754->getThreshold(&threshold) != AK9754::SUCCESS) {
                DBG_L1("#AK9754 Control - Error: Failed to get threshold for AK9754.\r\n");
                return  AkmSensor::ERROR;
            }
            out->setArgument(0,(char)(threshold.hbdthh));
            out->setArgument(1,(char)(threshold.hbdthl));
            break;
        }
        case Message::CMD_IR_SET_THRESHOLD:
        {
            threshold.hbdthh = in->getArgument(0);
            threshold.hbdthl = in->getArgument(1);

            if (ak9754->setThreshold(&threshold) != AK9754::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9754 Control - Error: Failed to set threshold of AK9754.\r\n");
            }

            out->setArgument(0, (char)status);

            break;
        }
        case Message::CMD_IR_GET_INTERRUPT:
        {
            if (ak9754->getInterruptEnable(&interruptEnable) != AK9754::SUCCESS) {
                DBG_L1("#AK9754 Control - Error: Failed to get enabled interrupts.\r\n");
                return  AkmSensor::ERROR;
            }

            out->setArgument(0, interruptEnable.drien);
            out->setArgument(1, interruptEnable.hbdien);

            break;
        }
        case Message::CMD_IR_SET_INTERRUPT:
        {
            interruptEnable.drien = in->getArgument(0);
            interruptEnable.hbdien = in->getArgument(1);

            if (ak9754->setInterruptEnable(&interruptEnable) != AK9754::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9754 Control - Error: Failed to set interrupts.\r\n");
            }

            out->setArgument(0, (char)status);

            break;            
        }
        case Message::CMD_IR_GET_OPERATION_MODE:
        {
            if(ak9754->getOperationMode(&mode) != AK9754::SUCCESS) {
                DBG_L1("#AK9754 Control - Error: Failed to get operation mode.\r\n");
                return  AkmSensor::ERROR;
            }

            out->setArgument(0, (char)mode);

            break;
        }
        case Message::CMD_IR_SET_OPERATION_MODE:
        {
            mode = (AK9754::OperationMode)in->getArgument(0);

            if(ak9754->setOperationMode(&mode) != AK9754::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9754 Control - Error: Failed to set operation mode.\r\n");
            }

            out->setArgument(0, (char)status);

            break;
        }
        case CMD_IR_GET_OPERATION_SETTINGS:
        {
			if(ak9754->getOperationSettings(&settings) != AK9754::SUCCESS) {
				DBG_L1("#AK9754 Control - Error: Failed to get operation settings.\r\n");
				return  AkmSensor::ERROR;
			}

            out->setArgument(0, (char)settings.low_noise);
            out->setArgument(1, (char)settings.odr);
            out->setArgument(2, (char)settings.fctmp);
            out->setArgument(3, (char)settings.fcir);
            out->setArgument(4, (char)settings.th_adj);
            out->setArgument(5, (char)settings.opt);
            out->setArgument(6, (char)settings.tmp_offset);
            out->setArgument(7, (char)settings.ir_gain);

            break;
        }
        case CMD_IR_SET_OPERATION_SETTINGS:
        {
        	settings.low_noise = (bool)in->getArgument(0);
			settings.odr = (AK9754::ODR)in->getArgument(1);
			settings.fctmp = (AK9754::DigitalFilter)in->getArgument(2);
			settings.fcir = (AK9754::DigitalFilter)in->getArgument(3);
			settings.th_adj = (bool)in->getArgument(4);
			settings.opt = (AK9754::Optimize)in->getArgument(5);
			settings.tmp_offset = in->getArgument(6);
			settings.ir_gain = in->getArgument(7);

			if(ak9754->setOperationSettings(&settings) != AK9754::SUCCESS) {
				status =  AkmSensor::ERROR;
				DBG_L1("#AK9754 Control - Error: Failed to set operation settings.\r\n");
			}

			out->setArgument(0, (char)status);

			break;
        }
        case CMD_IR_GET_HBD_SETTINGS:
        {
			if(ak9754->getHbdSettings(&hbdSettings) != AK9754::SUCCESS) {
				DBG_L1("#AK9754 Control - Error: Failed to get HBD settings.\r\n");
				return  AkmSensor::ERROR;
			}

			out->setArgument(0, (char)hbdSettings.hbd_en);
			out->setArgument(1, (char)hbdSettings.hbd_invert);
			out->setArgument(2, (char)hbdSettings.hbd_idle);
			out->setArgument(3, (char)hbdSettings.hbd_det_time);

        	break;
        }
        case CMD_IR_SET_HBD_SETTINGS:
        {
        	hbdSettings.hbd_en = (bool)in->getArgument(0);
			hbdSettings.hbd_invert = (bool)in->getArgument(1);
			hbdSettings.hbd_idle = (AK9754::HbdIdleTime)in->getArgument(2);
			hbdSettings.hbd_det_time = in->getArgument(3);

			if(ak9754->setHbdSettings(&hbdSettings) != AK9754::SUCCESS) {
				status =  AkmSensor::ERROR;
				DBG_L1("#AK9754 Control - Error: Failed to set HBD settings.\r\n");
			}

			out->setArgument(0, (char)status);

        	break;
        }
        case Message::CMD_REG_WRITE:
        case Message::CMD_REG_WRITEN:
        {
            char address = in->getArgument(0);
            const int len = (int)in->getArgument(1);
            char data[AK9754_LEN_BUF_MAX];

            if(in->getArgNum() != len+2){
                DBG_L1("#AK9754 Control - Error: Invalid number of arguments (args = %d).\r\n",in->getArgNum());
                status = AkmSensor::ERROR;
                out->setArgument(0, (char)status);
                return status;
            }

            for(int i = 0; i < len; i++){
                data[i] = in->getArgument(i+2);    
            }

            if( ak9754->write(address, data, len) != AK9754::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9754 Control - Error: Failed to write to register.\r\n");
            }

            out->setArgument(0,(char)status);

            break;
        }        
        case Message::CMD_REG_READ:
        case Message::CMD_REG_READN:
        {
            char address = in->getArgument(0);
            const int len = (int)in->getArgument(1);
            char data[AK9754_LEN_BUF_MAX];

            if(in->getArgNum() != 2){
                DBG_L1("#AK9754 Control - Error: Invalid number of arguments (args = %d)\r\n", in->getArgNum());
                return AkmSensor::ERROR;
            }

            if( ak9754->read(address, data, len) != AK9754::SUCCESS) {
                DBG_L1("#AK9754 Control - Error: Failed to read from register.\r\n");
                return AkmSensor::ERROR;
            }

            for(int i=0; i<len; i++){
                out->setArgument(i, data[i]);
            }

            break;
        }
        default:
        {
            DBG_L1("#AK9754 Control - Error: No command given.\r\n");
            status =  AkmSensor::ERROR;
            out->setArgument(0, (char)status);
            break;
        }
    }

    return status;
}
