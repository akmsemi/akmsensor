#ifndef AK9756CTRL_H
#define AK9756CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "AK9756.h"

#define CMD_IR_GET_OPERATION_SETTINGS	0x92
#define CMD_IR_SET_OPERATION_SETTINGS	0x93
#define CMD_IR_GET_ALGORITHM_SETTINGS	0x98
#define	CMD_IR_SET_ALGORITHM_SETTINGS	0x99

/**
 * Class for handling AKDP commands issued for the AK9756.
 */
class Ak9756Ctrl : public AkmSensor
{
public:

    /**
     * Device Sub-ID
     */
    typedef enum {
        SUB_ID_AK9756              = 0x06       /**< AK9756: 06h */
    } SubIdAk9756;

    /**
     * Constructor.
     *
     */
    Ak9756Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak9756Ctrl();

    /**
     * Initializes the device.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);

    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();

    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);

    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();

    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);

    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager.
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status requestCommand(Message* in, Message* out);

    /**
     * Set event flag.
     */
    virtual void setEvent();

private:
    AK9756*         				ak9756;

    AK9756::SensorData				data;
    AK9756::OperationMode			opMode;
    AK9756::OperationSettings		opSettings;
    AK9756::Threshold				threshold;
    AK9756::InterruptEnable			interruptEnable;
    AK9756::AlgorithmSettings		algSettings;
};


#endif /* AK9756CTRL_H */
