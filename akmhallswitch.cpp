#include "akmhallswitch.h"

///////////////////////////////////////////////////////////////////////////////////
// Interface implementation
///////////////////////////////////////////////////////////////////////////////////
/**
 * Constructor.
 *
 */
AkmHallSwitch::AkmHallSwitch() : AkmSensor(){
    sw0 = NULL;
    sw1 = NULL;
    d0 = DigitalIn(DIGITAL_D0);
    d1 = DigitalIn(DIGITAL_D1);
}

/**
 * Destructor.
 *
 */
AkmHallSwitch::~AkmHallSwitch(){    
    sw1->rise(0);
    sw1->fall(0);
    sw0->rise(0);
    sw0->fall(0);
    
    if(sw0) delete sw0;
    if(sw1) delete sw1;
}

AkmSensor::Status AkmHallSwitch::init(const uint8_t id, const uint8_t subid){
    primaryId = id;
    subId = subid;
    
    if(primaryId == AKM_PRIMARY_ID_UNIPOLAR && subId == AkmHallSwitch::SUB_ID_EM1771)         sensorName = "EM-1771";
    else if(primaryId == AKM_PRIMARY_ID_UNIPOLAR && subId == AkmHallSwitch::SUB_ID_EW453)    sensorName = "EW-453";
    else if(primaryId == AKM_PRIMARY_ID_UNIPOLAR && subId == AkmHallSwitch::SUB_ID_EW652B)    sensorName = "EW-652B";
    else if(primaryId == AKM_PRIMARY_ID_UNIPOLAR && subId == AkmHallSwitch::SUB_ID_EW6672)    sensorName = "EW-6672";
    else if(primaryId == AKM_PRIMARY_ID_OMNIPOLAR && subId == AkmHallSwitch::SUB_ID_EM1781)    sensorName = "EM-1781";
    else if(primaryId == AKM_PRIMARY_ID_OMNIPOLAR && subId == AkmHallSwitch::SUB_ID_AK8788A)    sensorName = "AK8788A";
    else if(primaryId == AKM_PRIMARY_ID_OMNIPOLAR && subId == AkmHallSwitch::SUB_ID_EM6781)    sensorName = "EM-6781";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_AK8771)    sensorName = "AK8771";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_EW432)    sensorName = "EW-432";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_EZ470)    sensorName = "EZ-470";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_EZ471)    sensorName = "EZ-471";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_EW612B)    sensorName = "EW-612B";
    else if(primaryId == AKM_PRIMARY_ID_LATCH && subId == AkmHallSwitch::SUB_ID_EW632)    sensorName = "EW-632";
    else if(primaryId == AKM_PRIMARY_ID_DUAL_OUTPUT && subId == AkmHallSwitch::SUB_ID_EM1791)    sensorName = "EM-1791";
    else if(primaryId == AKM_PRIMARY_ID_DUAL_OUTPUT && subId == AkmHallSwitch::SUB_ID_AK8789)    sensorName = "AK8789";
    else if(primaryId == AKM_PRIMARY_ID_ONECHIP_ENCODER && subId == AkmHallSwitch::SUB_ID_AK8775)    sensorName = "AK8775";
    else if(primaryId == AKM_PRIMARY_ID_ONECHIP_ENCODER && subId == AkmHallSwitch::SUB_ID_AK8776)    sensorName = "AK8776";
    else if(primaryId == AKM_PRIMARY_ID_ONECHIP_ENCODER && subId == AkmHallSwitch::SUB_ID_AK8779A)    sensorName = "AK8779A";
    else if(primaryId == AKM_PRIMARY_ID_ONECHIP_ENCODER && subId == AkmHallSwitch::SUB_ID_AK8779B)    sensorName = "AK8779B";

    sw0 = new InterruptIn(DIGITAL_D0);
    sw1 = new InterruptIn(DIGITAL_D1);
    
    sw1->rise(0);
    sw1->fall(0);
    sw0->rise(0);
    sw0->fall(0);
    
    return AkmSensor::SUCCESS;
}

void AkmHallSwitch::riseEventD0(){
    d0 = 1;
    AkmSensor::setEvent();
}
void AkmHallSwitch::fallEventD0(){
    d0 = 0;
    AkmSensor::setEvent();
}
void AkmHallSwitch::riseEventD1(){
    d1 = 1;
    AkmSensor::setEvent();
}

void AkmHallSwitch::fallEventD1(){
    d1 = 0;
    AkmSensor::setEvent();
}

AkmSensor::Status AkmHallSwitch::startSensor(){
    sw1->rise(callback(this, &AkmHallSwitch::riseEventD1));
    sw1->fall(callback(this, &AkmHallSwitch::fallEventD1));
    if(primaryId == AkmSensor::AKM_PRIMARY_ID_DUAL_OUTPUT || primaryId == AkmSensor::AKM_PRIMARY_ID_ONECHIP_ENCODER)
    {
        sw0->rise(callback(this, &AkmHallSwitch::riseEventD0));
        sw0->fall(callback(this, &AkmHallSwitch::fallEventD0));
    }
    DBG_L1("#Start sensor %s.\r\n",sensorName);
    return AkmSensor::SUCCESS;
}

AkmSensor::Status AkmHallSwitch::startSensor(const float sec){
    return AkmSensor::ERROR;
}

AkmSensor::Status AkmHallSwitch::stopSensor(){
    sw1->rise(0);
    sw1->fall(0);
    sw0->rise(0);
    sw0->fall(0);
    return AkmSensor::SUCCESS;
}

AkmSensor::Status AkmHallSwitch::readSensorData(Message* msg){
    AkmSensor::clearEvent();

    msg->setCommand(Message::CMD_START_MEASUREMENT);
    msg->setArgument( 0, d1 ? 1 : 0 );
    if(primaryId == AkmSensor::AKM_PRIMARY_ID_DUAL_OUTPUT || primaryId == AkmSensor::AKM_PRIMARY_ID_ONECHIP_ENCODER)
    {
        msg->setArgument( 1, d0 ? 1 : 0 );
    }
    return AkmSensor::SUCCESS;
}

AkmSensor::Status AkmHallSwitch::requestCommand(Message* in, Message* out){
    return AkmSensor::ERROR;
}
