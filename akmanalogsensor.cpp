#include "akmanalogsensor.h"


#define WAIT_ADC_MS 1

AkmAnalogSensor::AkmAnalogSensor() : AkmSensor(){
    ain = NULL;
#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    mcp3428 = NULL;
#endif
}


AkmAnalogSensor::~AkmAnalogSensor(){
    if(ain) delete ain;
#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_BLAB_BLENANO2)
    if(mcp3428) delete mcp3428;
#endif
}


AkmSensor::Status AkmAnalogSensor::init(const uint8_t p_id, const uint8_t s_id){
    
    DBG_L3("#Analog Sensor initialization.\r\n");
    
    primaryId = p_id;
    subId = s_id;
    
    if(primaryId == AKM_PRIMARY_ID_LINEAR_SENSOR && subId == AkmAnalogSensor::SUB_ID_EQ430L)             sensorName = "EQ-430L";
    else if(primaryId == AKM_PRIMARY_ID_LINEAR_SENSOR && subId == AkmAnalogSensor::SUB_ID_EQ431L)        sensorName = "EQ-431L";
    else if(primaryId == AKM_PRIMARY_ID_LINEAR_SENSOR && subId == AkmAnalogSensor::SUB_ID_EQ432L)        sensorName = "EQ-432L";
    else if(primaryId == AKM_PRIMARY_ID_LINEAR_SENSOR && subId == AkmAnalogSensor::SUB_ID_EQ433L)        sensorName = "EQ-433L";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ3200)    sensorName = "CQ-3200";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ3201)    sensorName = "CQ-3201";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ3202)    sensorName = "CQ-3202";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ3203)    sensorName = "CQ-3203";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ3204)    sensorName = "CQ-3204";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ320A)    sensorName = "CQ-320A";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V && subId == AkmAnalogSensor::SUB_ID_CQ320B)    sensorName = "CQ-320B";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ3300)    sensorName = "CQ-3300";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ3301)    sensorName = "CQ-3301";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ3302)    sensorName = "CQ-3302";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ3303)    sensorName = "CQ-3303";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330A)    sensorName = "CQ-330A";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330B)    sensorName = "CQ-330B";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330E)    sensorName = "CQ-330E";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330F)    sensorName = "CQ-330F";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330G)    sensorName = "CQ-330G";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330H)    sensorName = "CQ-330H";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CQ330J)    sensorName = "CQ-330J";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CZ3813)    sensorName = "CZ-3813";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CZ3814)    sensorName = "CZ-3814";
    else if(primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V && subId == AkmAnalogSensor::SUB_ID_CZ3815)    sensorName = "CZ-3815";
    else if(primaryId == AKM_PRIMARY_ID_MISC_ANALOG && subId == AkmAnalogSensor::SUB_ID_EM3242)          sensorName = "EM3242";
    else if(primaryId == AKM_PRIMARY_ID_MISC_ANALOG && subId == AkmAnalogSensor::SUB_ID_AK9710)          sensorName = "AK9710";
    else return AkmSensor::ERROR;

#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    MCP342X::AdcChannel channel = MCP342X::ADC_CH3;    // 0-5.0V out as default
    
    if( ((primaryId == AKM_PRIMARY_ID_MISC_ANALOG) && (subId <= AKM_PRIMARY_ID_CURRENT_SENSOR_3V))
       || primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V){
        channel = MCP342X::ADC_CH4;    // 0-3.0V out
    }
    
    // 16bit ADC
    I2C* i2c = new I2C(I2C_SDA,I2C_SCL);
    i2c->frequency(I2C_SPEED);
    DBG_L3("#AkmAnalogSensor::init: I2C created at %d Hz\r\n", I2C_SPEED);
    
    // ADC setting
    mcp3428 = new MCP342X(i2c, MCP342X::SLAVE_ADDRESS_6EH);
    mcp3428->setChannel(channel);
    mcp3428->setSampleSetting(MCP342X::SAMPLE_15HZ_16BIT);
    mcp3428->setConversionMode(MCP342X::CONTINUOUS); 
    DBG_L3("#AkmAnalogSensor::init: MPC342X ADC created\r\n");
#endif  // defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)

#ifdef TARGET_RAYTAC_MDBT42Q
    if( ((primaryId == AKM_PRIMARY_ID_MISC_ANALOG) && (subId <= AKM_PRIMARY_ID_CURRENT_SENSOR_3V))
       || primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V){
        
        ain = new AnalogIn(ANALOG_IN_A0);
        DBG_L2("#ADC Connected to 3V Channel\r\n");
    }
    else {
        ain = new AnalogIn(ANALOG_IN_A0);;
        DBG_L2("#ADC Connected to 5V Channel\r\n");
    }
#endif // TARGET_RAYTAC_MDBT42Q
    
    interval = SENSOR_SAMPLING_RATE; // 10Hz

    return AkmSensor::SUCCESS;
}


AkmSensor::Status AkmAnalogSensor::startSensor(){
    ticker.attach(callback(this, &AkmSensor::setEvent), interval);
    DBG_L1("#Start sensor: %s.\r\n",sensorName);
    return AkmSensor::SUCCESS;
}


AkmSensor::Status AkmAnalogSensor::startSensor(const float sec){
    interval = sec;
    ticker.attach(callback(this, &AkmSensor::setEvent), interval);
    DBG_L1("#Start sensor: %s (Interval = %.2f)\r\n", sensorName, interval);
    return AkmSensor::SUCCESS;
}


AkmSensor::Status AkmAnalogSensor::stopSensor(){
    ticker.detach();
    DBG_L1("#Stop sensor: %s.\r\n", sensorName);
    return AkmSensor::SUCCESS;
}


AkmSensor::Status AkmAnalogSensor::readSensorData(Message* msg){
   
    DBG_L3("#Reading analog sensor data.\r\n");
    AkmSensor::clearEvent();
    
    msg->setCommand(Message::CMD_START_MEASUREMENT);

    if(primaryId == AKM_PRIMARY_ID_LINEAR_SENSOR || primaryId == AKM_PRIMARY_ID_MISC_ANALOG || 
       primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_3V || primaryId == AKM_PRIMARY_ID_CURRENT_SENSOR_5V )
    {
#ifdef TARGET_RAYTAC_MDBT42Q
        uint16_t ain16 = 0;

        // Sample the ADC
        ain16 = ain->read_u16();

        DBG_L2("#ADC Raw Input = %d\t\r\n", ain16);
        
        msg->setArgument(0, (char)((ain16 & 0xFF00) >> 8));
        msg->setArgument(1, (char)(ain16 & 0x00FF));
#endif // TARGET_RAYTAC_MDBT42Q

#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
        MCP342X::Data data;

        do {
            DBG_L3("#Waiting..\r\n");
            mcp3428->getData(&data);
            wait_ms(WAIT_ADC_MS);
        } while(data.st == MCP342X::DATA_NOT_UPDATED);
        
        DBG_L2("#Sensor Data: %d\r\n", data.value);
        
        msg->setArgument( 0, (char)((data.value & 0xFF00) >> 8) );
        msg->setArgument( 1, (char)(data.value & 0x00FF) );
#endif // defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    }
    else{
        msg->setArgument(0, 0);
        msg->setArgument(1, 0);
        DBG_L1("Error: Failed to read sensor data\r\n");

        return AkmSensor::ERROR;
    }
    return AkmSensor::SUCCESS;
}


AkmSensor::Status AkmAnalogSensor::requestCommand(Message* in, Message* out){
    DBG_L1("Error: Failed to request command.\r\n");
    return AkmSensor::ERROR;
}
