#include "ak9756ctrl.h"
#include "AK9756_reg.h"
#include "AK9756.h"

#define CONV16I(high,low)  ((int16_t)(((high) << 8) | (low)))


Ak9756Ctrl::Ak9756Ctrl() : AkmSensor(){
    ak9756 = NULL;
}

Ak9756Ctrl::~Ak9756Ctrl(){
    if (ak9756) delete ak9756;
}

AkmSensor::Status Ak9756Ctrl::init(const uint8_t id, const uint8_t subid){

	primaryId = id;
    subId = subid;
    bool foundSensor = false;

    AK9756::SlaveAddress slaveAddr[] = { AK9756::SLAVE_ADDR_1 };

    I2C* i2c = new I2C(I2C_SDA,I2C_SCL);
    i2c->frequency(I2C_SPEED);

    if(subId == SUB_ID_AK9756){
		ak9756 = new AK9756();
		sensorName = "AK9756";
    }
    else{
    	DBG_L1("#AK9756 Control - Error: Invalid Sub-ID.\r\n");
        return AkmSensor::ERROR;
    }

    for(unsigned int i=0; i < sizeof(slaveAddr); i++)
    {
        ak9756->init(i2c, slaveAddr[i]);

        if(ak9756->verifyDevice() == AK9756::SUCCESS) {
            foundSensor = true;
            break;
        }
    }

    if(foundSensor != true){
    	DBG_L1("#AK9756 Control - Error: No sensor found.\r\n");
    	return AkmSensor::ERROR;
    }

    // reset
    if (ak9756->reset() != AK9756::SUCCESS) {
        DBG_L1("#AK9756 Control - Error: Failed to reset AK9756.\r\n");
    }

    DBG_L3("#AK9756 Control - Initialization success.\r\n");
    return AkmSensor::SUCCESS;
}

void Ak9756Ctrl::setEvent(){
    DBG_L3("#AK9756 Control - setEvent().\r\n");

    AK9756::Status status;

    status = ak9756->isDataReady();

    if(status == AK9756::DATA_READY)
    	AkmSensor::setEvent();
}

AkmSensor::Status Ak9756Ctrl::startSensor(){

	opMode = AK9756::OPERATION_MODE_CONTINUOUS;

    // Read data once to clear interrupt
	ak9756->getSensorData(&data);

	// Set operation and algorithm settings
	ak9756->setOperationSettings(&opSettings);
	ak9756->setAlgorithmSettings(&algSettings);

    if(ak9756->setOperationMode(&opMode) != AK9756::SUCCESS) {
        DBG_L1("#AK9756 Control - Error: Start sensor failed %s\r\n", sensorName);
        return AkmSensor::ERROR;
    }

    DBG_L3("#AK9756 Control - Sensor started\r\n");
    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9756Ctrl::startSensor(const float sec){
    return AkmSensor::ERROR;
}

AkmSensor::Status Ak9756Ctrl::stopSensor(){

	opMode = AK9756::OPERATION_MODE_STANDBY;

	AkmSensor::clearEvent();

    if(ak9756->setOperationMode(&opMode) != AK9756::SUCCESS) {
        DBG_L1("#AK9756 Control - Error: Unable to set operation mode.\r\n");
        return AkmSensor::ERROR;
    }

    // Read data once to clear interrupt
    ak9756->getSensorData(&data);

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9756Ctrl::readSensorData(Message* msg){

    AkmSensor::clearEvent();

    if(ak9756->getSensorData(&data) != AK9756::SUCCESS){
        DBG_L1("#AK9756 Control - Error: Unable to get sensor data.\r\n");
        return AkmSensor::ERROR;
    }

    msg->setCommand(Message::CMD_START_MEASUREMENT);
    msg->setArgument(0, (char)data.intStatus.drdy);
    msg->setArgument(1, (char)data.intStatus.h01);
    msg->setArgument(2, (char)data.intStatus.h10);
    msg->setArgument(3, (char)data.intStatus.difthh);
    msg->setArgument(4, (char)data.intStatus.difthl);
    msg->setArgument(5, (char)(((uint16_t)data.ir1) >> 8));
    msg->setArgument(6, (char)(((uint16_t)data.ir1) & 0x00FF) );
    msg->setArgument(7, (char)(((uint16_t)data.ir2) >> 8));
    msg->setArgument(8, (char)(((uint16_t)data.ir2) & 0x00FF) );
    msg->setArgument(9, (char)(((uint16_t)data.temperature) >> 8));
    msg->setArgument(10, (char)(((uint16_t)data.temperature) & 0x00FF));
    msg->setArgument(11, (char)data.hrslt);
    msg->setArgument(12, (char)data.dor);

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak9756Ctrl::requestCommand(Message* in, Message* out){

	AkmSensor::Status status = AkmSensor::SUCCESS;
    Message::Command cmd;

    cmd = in->getCommand();

    out->setCommand(cmd);

    switch(cmd){
        case Message::CMD_IR_GET_THRESHOLD:
        {
            if (ak9756->getThreshold(&threshold) != AK9756::SUCCESS) {
                status = AkmSensor::ERROR;
            	DBG_L1("#AK9756 Control - Error: Failed to get threshold for AK9756.\r\n");
                out->setArgument(0, (char)status);
                break;
            }
            out->setArgument(0, (char)(threshold.diff_thh >> 8));
            out->setArgument(1, (char)(threshold.diff_thh & 0xFF));
            out->setArgument(2, (char)(threshold.diff_thl >> 8));
            out->setArgument(3, (char)(threshold.diff_thl & 0xFF));
            out->setArgument(4, (char)(threshold.sum_thh >> 8));
            out->setArgument(5, (char)(threshold.sum_thh & 0xFF));
            out->setArgument(6, (char)(threshold.sum_thl >> 8));
            out->setArgument(7, (char)(threshold.sum_thl & 0xFF));

            break;
        }
        case Message::CMD_IR_SET_THRESHOLD:
        {
            threshold.diff_thh = in->getArgument(0);
            threshold.diff_thh |= in->getArgument(1) << 8;
            threshold.diff_thl = in->getArgument(2);
            threshold.diff_thl |= in->getArgument(3) << 8;
            threshold.sum_thh = in->getArgument(4);
            threshold.sum_thh |= in->getArgument(5) << 8;
            threshold.sum_thl = in->getArgument(6);
            threshold.sum_thl |= in->getArgument(7) << 8;

            if (ak9756->setThreshold(&threshold) != AK9756::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9756 Control - Error: Failed to set threshold of AK9756.\r\n");
            }

            out->setArgument(0, (char)status);

            break;
        }
        case Message::CMD_IR_GET_INTERRUPT:
        {
            if (ak9756->getInterruptEnable(&interruptEnable) != AK9756::SUCCESS) {
                status = AkmSensor::ERROR;
            	DBG_L1("#AK9756 Control - Error: Failed to get enabled interrupts.\r\n");
                out->setArgument(0, (char)status);
                break;
            }

            out->setArgument(0, interruptEnable.drien);
            out->setArgument(1, interruptEnable.diffhlie);
            out->setArgument(2, interruptEnable.difflhie);
            out->setArgument(3, interruptEnable.wmie);
            out->setArgument(4, interruptEnable.hie);
            out->setArgument(5, interruptEnable.h10ie);
            out->setArgument(6, interruptEnable.h01ie);
            out->setArgument(7, interruptEnable.histat);

            break;
        }
        case Message::CMD_IR_SET_INTERRUPT:
        {
            interruptEnable.drien = in->getArgument(0);
            interruptEnable.diffhlie = in->getArgument(1);
            interruptEnable.difflhie = in->getArgument(2);
            interruptEnable.wmie = in->getArgument(3);
            interruptEnable.hie = in->getArgument(4);
            interruptEnable.h10ie = in->getArgument(5);
            interruptEnable.h01ie = in->getArgument(6);
            interruptEnable.histat = in->getArgument(7);

            if (ak9756->setInterruptEnable(&interruptEnable) != AK9756::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9756 Control - Error: Failed to set interrupts.\r\n");
            }

            out->setArgument(0, (char)status);

            break;
        }
        case Message::CMD_IR_GET_OPERATION_MODE:
        {
            if(ak9756->getOperationMode(&opMode) != AK9756::SUCCESS) {
                DBG_L1("#AK9756 Control - Error: Failed to get operation mode.\r\n");
                status = AkmSensor::ERROR;
                out->setArgument(0, (char)status);
                break;
            }

            out->setArgument(0, (char)opMode);

            break;
        }
        case Message::CMD_IR_SET_OPERATION_MODE:
        {
            opMode = (AK9756::OperationMode)in->getArgument(0);

            if(ak9756->setOperationMode(&opMode) != AK9756::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9756 Control - Error: Failed to set operation mode.\r\n");
            }

            out->setArgument(0,(char)status);

            break;
        }
        case CMD_IR_GET_OPERATION_SETTINGS:
        {
        	if(ak9756->getOperationSettings(&opSettings) != AK9756::SUCCESS) {
				DBG_L1("#AK9756 Control - Error: Failed to get operation settings.\r\n");
				status = AkmSensor::ERROR;
				out->setArgument(0, (char)status);
				break;
			}

        	out->setArgument(0, (char)opSettings.measure_mode);
			out->setArgument(1, (char)opSettings.odr);
			out->setArgument(2, (char)opSettings.fc_ir);
			out->setArgument(3, (char)opSettings.fc_tmp);
			out->setArgument(4, (char)opSettings.ir_gain);

        	break;
        }
        case CMD_IR_SET_OPERATION_SETTINGS:
		{
			opSettings.measure_mode = (AK9756::MeasurementMode)in->getArgument(0);
			opSettings.odr = (AK9756::ODR)in->getArgument(1);
			opSettings.fc_ir = (AK9756::DigitalFilter)in->getArgument(2);
			opSettings.fc_tmp = (AK9756::DigitalFilter)in->getArgument(3);
			opSettings.ir_gain = (uint8_t)in->getArgument(4);

			if(ak9756->setOperationSettings(&opSettings) != AK9756::SUCCESS) {
				status =  AkmSensor::ERROR;
				DBG_L1("#AK9756 Control - Error: Failed to set operation settings.\r\n");
			}

			out->setArgument(0,(char)status);

			break;
		}
        case CMD_IR_GET_ALGORITHM_SETTINGS:
        {
        	if(ak9756->getAlgorithmSettings(&algSettings) != AK9756::SUCCESS){
        		status = AkmSensor::ERROR;
        		DBG_L1("#AK9756 Control - Error: Failed to get algorithm settings.\r\n");
        		out->setArgument(0, (char)status);
        		break;
        	}

        	out->setArgument(0, (char)algSettings.appr_sum_en);
        	out->setArgument(1, (char)algSettings.appr_diff_en);
        	out->setArgument(2, (char)algSettings.appr_sum_tc);
        	out->setArgument(3, (char)algSettings.appr_diff_tc);
        	out->setArgument(4, (char)algSettings.appr_count);
        	out->setArgument(5, (char)algSettings.dprt_sum_en);
        	out->setArgument(6, (char)algSettings.dprt_diff_en);
        	out->setArgument(7, (char)algSettings.dprt_sum_tc);
        	out->setArgument(8, (char)algSettings.dprt_diff_tc);
        	out->setArgument(9, (char)algSettings.dprt_time);

        	break;
        }
        case CMD_IR_SET_ALGORITHM_SETTINGS:
        {
        	algSettings.appr_sum_en = (bool)in->getArgument(0);
        	algSettings.appr_diff_en = (bool)in->getArgument(1);
        	algSettings.appr_sum_tc = (AK9756::SumTimeConstant)in->getArgument(2);
        	algSettings.appr_diff_tc = (AK9756::DiffTimeConstant)in->getArgument(3);
        	algSettings.appr_count = (uint8_t)in->getArgument(4);
        	algSettings.dprt_sum_en = (bool)in->getArgument(5);
        	algSettings.dprt_diff_en = (bool)in->getArgument(6);
        	algSettings.dprt_sum_tc = (AK9756::SumTimeConstant)in->getArgument(7);
        	algSettings.dprt_diff_tc = (AK9756::DiffTimeConstant)in->getArgument(8);
        	algSettings.dprt_time = (uint8_t)in->getArgument(9);

        	if(ak9756->setAlgorithmSettings(&algSettings) != AK9756::SUCCESS){
        		status = AkmSensor::ERROR;
        		DBG_L1("#AK9756 Control - Error: Failed to set algorithm settings.\r\n");
        	}

        	out->setArgument(0, (char)status);

        	break;
        }
        case Message::CMD_REG_WRITE:
        case Message::CMD_REG_WRITEN:
        {
            char address = in->getArgument(0);
            const int len = (int)in->getArgument(1);
            char data[AK9756_LEN_BUF_MAX];

            if(in->getArgNum() != len+2){
                DBG_L1("#AK9756 Control - Error: Invalid number of arguments (args = %d).\r\n",in->getArgNum());
                status = AkmSensor::ERROR;
                out->setArgument(0,(char)status);
                return status;
            }

            for(int i = 0; i < len; i++){
                data[i] = in->getArgument(i+2);
            }

            if( ak9756->write(address, data, len) != AK9756::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#AK9756 Control - Error: Failed to write to register.\r\n");
            }

            out->setArgument(0,(char)status);

            break;
        }
        case Message::CMD_REG_READ:
        case Message::CMD_REG_READN:
        {
            char address = in->getArgument(0);
            const int len = (int)in->getArgument(1);
            char data[AK9756_LEN_BUF_MAX];

            if(in->getArgNum() != 2){
                DBG_L1("#AK9756 Control - Error: Invalid number of arguments (args = %d)\r\n", in->getArgNum());
                return AkmSensor::ERROR;
            }

            if( ak9756->read(address, data, len) != AK9756::SUCCESS) {
                DBG_L1("#AK9756 Control - Error: Failed to read from register.\r\n");
                return AkmSensor::ERROR;
            }

            for(int i=0; i<len; i++){
                out->setArgument(i, data[i]);
            }

            break;
        }
        default:
        {
            DBG_L1("#AK9756 Control - Error: No command given.\r\n");
            status =  AkmSensor::ERROR;
            out->setArgument(0, (char)status);
            break;
        }
    }

    return status;
}
