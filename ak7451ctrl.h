#ifndef AK7451CTRL_H
#define AK7451CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "ak7451.h"

/**
 * Class for handling commands issued to the AK7451.
 */
class Ak7451Ctrl : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /**
     * Device Sub-ID.
     */
    typedef enum {
        SUB_ID_AK7451              = 0x01,          /**< AK7451: ID = 01h */
    } SubIdAngleSensor;

    /**
     * Constructor.
     *
     */
    Ak7451Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak7451Ctrl();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
   
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);

private:
    Ticker          ticker;
    AK7451*         ak7451;
    float           interval;
};

#endif
