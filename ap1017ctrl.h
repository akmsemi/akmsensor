#ifndef AP1017CTRL_H
#define AP1017CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "AP1017.h"

/**
 * Class for handling commands issued to the AP1017 motor driver.
 */
class Ap1017Ctrl : public AkmSensor
{
public:

    /**
     * Device Sub-ID.
     */
    typedef enum {
        SUB_ID_AP1017              = 0x02,          /**< AP1017: ID = 02h (5bit ID)*/
    } SubIdMotorDriver;
    
    // Constructors and Destructors
    Ap1017Ctrl();
    virtual ~Ap1017Ctrl();

    // Public Member Functions
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Simple flag process to determine if an event has occurred.
     *
     * @return TRUE if event has occurred, FALSE if not.
     */
    virtual bool isEvent();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param freq PWM frequency in Hz (2000 Hz by default).
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float freq);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);
    
    /**
     * Get the name of the sensor in char format.
     *
     * @return Sensor name as a char array.
     */
    virtual const char* getSensorName();
    
    // Interrupt Service Routines
    void pwmPeriod();
    void pwmOnPulse();

private:
    uint8_t         primaryId;
    uint8_t         subId;
    const char*     sensorName;

    DigitalOut*     inputA;
    DigitalOut*     inputB;
    DigitalOut*     enable;

    AP1017*         ap1017;
    Ticker          pwm;
    Ticker          pulse;
    uint8_t         index;
    float           freq;
    float           interval;       // Timer interrupt interval
};

#endif