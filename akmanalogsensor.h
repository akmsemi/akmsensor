#ifndef AKMANALOGSENSOR_H
#define AKMANALOGSENSOR_H

#include "mbed.h"
#include "akmsensor.h"
#include "akdphwinfo.h"

/**
 * Collection class for handling commands to all AKM Analog Sensor modules.
 *
 * List of Devices: 
 *
 * Current Sensors CQ32xx: CQ3200, CQ3201, CQ3202, CQ3203, CQ3204, CQ320A, CQ320B,
 * 
 * Current Sensors CQ33xx: CQ3300, CQ3301, CQ3302, CQ3303, CQ330A, CQ330B, CQ330E,
 * CQ330F, CQ330G, CQ330H, CQ330J
 * 
 * Linear Sensors: EQ430L, EQ431L, EQ432L, EQ433L
 *
 * Angle Sensors: EM3242
 *
 * IR/CO2 Sensor: AK9710
 */
class AkmAnalogSensor : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /**
     * Sub-IDs for current sensor CQ32xx devices. Primary ID = 07h
     */
    typedef enum {
        SUB_ID_CQ3200               = 0x01,         /**< CQ3200: ID = 01h */
        SUB_ID_CQ3201               = 0x02,         /**< CQ3201: ID = 02h */
        SUB_ID_CQ3202               = 0x03,         /**< CQ3202: ID = 03h */
        SUB_ID_CQ3203               = 0x04,         /**< CQ3203: ID = 04h */
        SUB_ID_CQ3204               = 0x05,         /**< CQ3204: ID = 05h */
        SUB_ID_CQ320A               = 0x06,         /**< CQ320A: ID = 06h */
        SUB_ID_CQ320B               = 0x07,         /**< CQ320B: ID = 07h */
    } SubIdCurrentSensor3v;
    
    /**
     * Sub-IDs for current sensor CQ33xx devices. Primary ID = 09h
     */
    typedef enum {
        SUB_ID_CQ3300               = 0x01,         /**< CQ3300: ID = 01h */
        SUB_ID_CQ3301               = 0x02,         /**< CQ3301: ID = 02h */
        SUB_ID_CQ3302               = 0x03,         /**< CQ3302: ID = 03h */
        SUB_ID_CQ3303               = 0x04,         /**< CQ3303: ID = 04h */
        SUB_ID_CQ330A               = 0x05,         /**< CQ330A: ID = 05h */
        SUB_ID_CQ330B               = 0x06,         /**< CQ330B: ID = 06h */
        SUB_ID_CQ330E               = 0x07,         /**< CQ330E: ID = 07h */
        SUB_ID_CQ330F               = 0x08,         /**< CQ330F: ID = 08h */
        SUB_ID_CQ330G               = 0x09,         /**< CQ330G: ID = 09h */
        SUB_ID_CQ330H               = 0x0A,         /**< CQ330H: ID = 0Ah */
        SUB_ID_CQ330J               = 0x0B,         /**< CQ330J: ID = 0Bh */
        SUB_ID_CZ3813               = 0x0D,         /**< CQ3813: ID = 0Dh */
        SUB_ID_CZ3814               = 0x0E,         /**< CQ3814: ID = 0Eh */
        SUB_ID_CZ3815               = 0x0F,         /**< CQ3815: ID = 0Fh */
    } SubIdCurrentSensor5v;

    /**
     * Sub-IDs for miscellaneous devices. Primary ID = 0Ah
     */
    typedef enum {
        SUB_ID_EM3242               = 0x01,         /**< EM3242: ID = 01h */
        SUB_ID_AK9710               = 0x08,         /**< AK9710: ID = 08h */
    } SubIdMisc;

    /**
     * Sub-IDs for analog devices. Primary ID = 0Bh
     */
    typedef enum {
        SUB_ID_EQ430L               = 0x01,         /**< EQ430L: ID = 01h */
        SUB_ID_EQ431L               = 0x02,         /**< EQ431L: ID = 02h */
        SUB_ID_EQ432L               = 0x03,         /**< EQ432L: ID = 03h */
        SUB_ID_EQ433L               = 0x04,         /**< EQ433L: ID = 04h */
    } SubIdLinearSensor;

    /**
     * Constructor.
     *
     */
    AkmAnalogSensor();

    /**
     * Destructor.
     *
     */
    virtual ~AkmAnalogSensor();
    
    /**
     * Process for initializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t p_id, const uint8_t s_id);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);

private: 
    Ticker          ticker;
    AnalogIn*       ain;
    AnalogIn*       ain2;
    float           interval;
#if defined(TARGET_RBLAB_BLENANO) || defined(TARGET_RBLAB_BLENANO2)
    MCP342X*        mcp3428;
#endif
};

#endif // AKMANALOGSENSOR_H
