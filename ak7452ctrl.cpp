#include "ak7452ctrl.h"

/**
 * Constructor.
 *
 */
Ak7452Ctrl::Ak7452Ctrl() : AkmSensor(){
    ak7452 = NULL;
}

/**
 * Destructor.
 *
 */
Ak7452Ctrl::~Ak7452Ctrl(){
    if (ak7452) delete ak7452;
}

AkmSensor::Status Ak7452Ctrl::init(const uint8_t id, const uint8_t subid){
    primaryId = id;
    subId = subid;

    if(subId == SUB_ID_AK7452){

    	char data[2] = {0x00, 0x00};

        SPI* mSpi;          // SPI connection
        DigitalOut* mCs;    // Chip select pin

        // Initialize SPI connection
        AK7452::Status status = AK7452::ERROR;
        mSpi = new SPI(SPI_MOSI, SPI_MISO, SPI_SCK);
        mSpi->format(8,1);    // 8bit, Mode=1
        mSpi->frequency(SPI_SPEED);

        // Initialize chip select pin
        mCs = new DigitalOut(SPI_CS);
        ak7452 = new AK7452();
        ak7452->begin(mSpi, mCs);
        sensorName = "AK7452";

        // Set to User Mode
        status = ak7452->setOperationMode(AK7452::AK7452_USER_MODE);
        if( status != AK7452::SUCCESS ){
        	DBG_L1("#Error: Failed to set User Mode.\r\n");
            return AkmSensor::ERROR;
        }
        DBG_L3("#AK7452: User Mode enabled.\r\n");

        // Initialize E_RDABZ register to:
        // E_RD = Clockwise rotation (0b1)
        // E_Z_WIDTH = 1LSB mode (0b00)
        // E_ABZ_E = ABZ Output enable (0b1)
        // E_ABZ_HYS = 1LSB ABZ Hysteresis (0b010)
        data[0] = 0x00;
        data[1] = 0xA0;

        status = ak7452->writeEEPROM(AK7452_EEPROM_RDABZ, data);
        if( status != AK7452::SUCCESS ){
            DBG_L1("#Error: Setting E_RDABZ EEPROM failed.\r\n");
            return AkmSensor::ERROR;
        }

        // Initialize E_ABZRES register to:
        // E_ABZRES = 1024ppr ABZ phase resolution (0x3FF)
        data[0] = 0xFF;
        data[1] = 0x03;
        status = ak7452->writeEEPROM(AK7452_EEPROM_ABZRES, data);
        if( status != AK7452::SUCCESS ){
            DBG_L1("#Error: Setting E_ABZRES EEPROM failed.\r\n");
            return AkmSensor::ERROR;
        }

        interval = SENSOR_SAMPLING_RATE; // 10Hz
    }
    else{
    	DBG_L1("Error: Failed to initialize AK7452.\r\n");
        return AkmSensor::ERROR;
    }

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak7452Ctrl::startSensor(){

	AK7452::Status status = AK7452::SUCCESS;

	if(ak7452->getOperationMode() != AK7452::AK7452_NORMAL_MODE){
		status = ak7452->setOperationMode(AK7452::AK7452_NORMAL_MODE);
	}

    if( status != AK7452::SUCCESS ){
        DBG_L1("#Error: Failed to set Normal Mode.\r\n");
        return AkmSensor::ERROR;
    }

    ticker.attach(callback(this, &AkmSensor::setEvent), interval);

    DBG_L2("#Sensor started: %s.\r\n", sensorName);

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak7452Ctrl::startSensor(const float sec){

    interval = sec;
    startSensor();

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak7452Ctrl::stopSensor(){

	AK7452::Status status;

	status = ak7452->setOperationMode(AK7452::AK7452_USER_MODE);

	if( status != AK7452::SUCCESS ){
        DBG_L1("#Error: AK7452 failed to set User Mode.\r\n");
        return AkmSensor::ERROR;
    }

	ticker.detach();
	AkmSensor::clearEvent();

    DBG_L2("#Sensor stopped: %s.\r\n", sensorName);

	return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak7452Ctrl::readSensorData(Message* msg){

    char angle[2] = {0x00, 0x00};
    AK7452::Status status;

    AkmSensor::clearEvent();

    status = ak7452->readAngle(angle);

    msg->setCommand(Message::CMD_START_MEASUREMENT);
    msg->setArgument(0, status);
    msg->setArgument(1, angle[0]);
    msg->setArgument(2, angle[1]);

    if( status != AK7452::SUCCESS){
    	DBG_L1("#Error: Could not read angle.\r\n");
        return AkmSensor::ERROR;
    }

    return AkmSensor::SUCCESS;
}

AkmSensor::Status Ak7452Ctrl::requestCommand(Message* in, Message* out){

	AkmSensor::Status status = AkmSensor::SUCCESS;
    Message::Command cmd;

    cmd = in->getCommand();

    switch(cmd){
         case Message::CMD_ANGLE_READ:
         {
            // read angle
            char angle[2] = {0x00, 0x00};
            char density[2] = {0x00, 0x00};
            char abnormal = 0x00;

            if( ak7452->readAngleFluxState(angle, density, &abnormal) != AK7452::SUCCESS ){
                DBG_L1("#Error: Failed to read angle data\r\n");
                status =  AkmSensor::ERROR;
            }

            out->setCommand(Message::CMD_ANGLE_READ);
            out->setArgument( 0, (abnormal != 0x03)|(status == AkmSensor::ERROR) ? 0x01 : 0x00 );
            out->setArgument( 1, angle[0] );
            out->setArgument( 2, angle[1] );
            out->setArgument( 3, density[0] );
            out->setArgument( 4, density[1] );

            break;
        }
         case Message::CMD_ANGLE_ZERO_RESET:
         {
            AK7452::Status st;
/*
            AK7452::OperationMode mode = AK7452::AK7452_USER_MODE;

            // check the mode
            char data[1];
            if( ak7452->readRegister(0x00, data) != AK7452::SUCCESS) {
                DBG_L1("#NORMAL MODE detected. Set into USER mode.\r\n");
                mode = AK7452::AK7452_NORMAL_MODE;
                st = ak7452->setOperationMode(AK7452::AK7452_USER_MODE);
                if( st != AK7452::SUCCESS ){
                    DBG_L1("#Error: when set user mode\r\n");
                    status = AkmSensor::ERROR;
                }
            }
*/
            st = ak7452->setAngleZero();    // reset ZP data

            if( st != AK7452::SUCCESS ){
                DBG_L1("#Error: setAngleZero: code=%d\r\n",st);
                status =  AkmSensor::ERROR;
            }
/*
            st = ak7452->setOperationMode(mode);
            if( st != AK7452::SUCCESS ){
                DBG_L1("#Error: when set mode:%d\r\n",mode);
                status =  AkmSensor::ERROR;
            }
*/
            if( status == AkmSensor::ERROR ){
                out->setArgument(0,1);
            }else{
                out->setArgument(0,0);
            }

            break;
        }
        case Message::CMD_REG_WRITE:
        case Message::CMD_REG_WRITEN:
        {
        	char data[AK7452_LEN_BUF_MAX];
            char address = in->getArgument(0);
            const int len = in->getArgument(1);

            if(len != 2){
                DBG_L1("#Error: length = %d - Command must be length of 2\r\n", len);
                status = AkmSensor::ERROR;
                return status;
            }
            if(in->getArgNum() != len+2){
                DBG_L1("#Error: args = %d - Invalid number of arguments\r\n", in->getArgNum());
                status = AkmSensor::ERROR;
                out->setArgument(0,(char)status);
                return status;
            }

            for(int i=0; i<len; i++){
                data[i] = in->getArgument(i+2);
            }

            if( ak7452->writeRegister(address, data) != AK7452::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#Error: Failed to write to register.\r\n");
            }
            if( ak7452->writeEEPROM(address, data) != AK7452::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#Error: Failed to write to EEPROM.\r\n");
            }

            out->setArgument(0,(char)status);

            break;
        }
        case Message::CMD_REG_READ:
        case Message::CMD_REG_READN:
        {
        	char data[AK7452_LEN_BUF_MAX];
        	char address;
        	int len;

            if(in->getArgNum() != 2){
                DBG_L1("#Error: argument num. Args=%d\r\n",in->getArgNum());
                status = AkmSensor::ERROR;
                return status;
            }

            address = in->getArgument(0);
            len = in->getArgument(1);

            if(len != 2){
                DBG_L1("#Error: length=%d. Only support 2byte length\r\n",len);
                status = AkmSensor::ERROR;
                return status;
            }
            if( ak7452->readRegister(address, data) != AK7452::SUCCESS) {
                status =  AkmSensor::ERROR;
                DBG_L1("#Error: register read.\r\n");
            }

            for(int i = 0; i < len; i++){
                out->setArgument(i, data[i]);
            }

            break;
        }
        default:
        {
            DBG_L1("#Error: No command.\r\n");
            status =  AkmSensor::ERROR;

            break;
        }
    }

    return status;
}
