#ifndef AKMAKD_H
#define AKMAKD_H

#include "mbed.h"
#include "akmsensor.h"
#include "akmecompass.h"

#define AKMAKD_LEN_OPT_MAX				2
#define AKDP_POLLING_FREQUENCY          (10.0)  // [Hz]

/**
 * Collection class for handling commands to all of the specialty AKM 
 * daughter board adapter modules.
 *
 * 3-Axis Electronic Compass Devices: AK8963C, AK8963N, AK09911C, AK09912C,
 * AK09915C, AK09915D, AK09916C, AK09916D, AK09917, AK09918, AK09940
 */
class AkmAkd : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /**
     * List of daughter board adapter devices (5-bit Sub-IDs). Primary ID = 0Fh
     */
    typedef enum {
        SUB_ID_AK8963N              = 0x1A,  /**< AK8963N:  ID = 1Ah (26) */
        SUB_ID_AK8963C              = 0x1C,  /**< AK8963C:  ID = 1Ch (28) */
        SUB_ID_AK09911C             = 0x0A,  /**< AK09911C: ID = 0Ah (10) */
        SUB_ID_AK09912C             = 0x09,  /**< AK09912C: ID = 09h (9)  */
        SUB_ID_AK09915C             = 0x0D,  /**< AK09915C: ID = 0Dh (13) */
        SUB_ID_AK09916C             = 0x0E,  /**< AK09916C: ID = 0Eh (14) */
        SUB_ID_AK09916D             = 0x0F,  /**< AK09916D: ID = 0Fh (15) */
        SUB_ID_AK09915D             = 0x10,  /**< AK09915D: ID = 10h (16) */
        SUB_ID_AK09918              = 0x11,  /**< AK09918:  ID = 11h (17) */
        SUB_ID_AK09917              = 0x12,  /**< AK09917:  ID = 12h (18) */
        SUB_ID_AK09940              = 0x13,  /**< AK09940:  ID = 13h (19) */
    } SubIdAkd;
    
    typedef enum {
        INTERRUPT_DISABLED          = 0x00, // Polling
        INTERRUPT_ENABLED_PP        = 0x01, // Push-Pull
        INTERRUPT_ENABLED_OD        = 0x02, // Open drain
    } InterruptMode;
    
    /**
     * Constructor.
     *
     */
    AkmAkd();

    /**
     * Destructor.
     *
     */
    virtual ~AkmAkd();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);

    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);
    
    /**
     * Set event flag.
     */
    virtual void setEvent();

    /**
     * Unimplemented
     */
    int getSensorType();

    /**
     * Returns the interrupt mode of the device.
     *
     * @param id1 Primary ID of the device
     * @param id2 Secondary ID of the device
     * @return Interrupt mode: PP enabled, OD enabled, or disabled
     */
    InterruptMode getInterrupt(uint8_t primaryId, uint8_t subId);

    /**
     * Creates an instance of the device and sets compass pointer to the new device.
     *
     * @param id1 Primary ID of the device
     * @param id2 Secondary ID of the device
     * @param devid Pointer to the ID of the newly created device instance
     * @return Success or failure of the process
     */
    AkmSensor::Status checkSensor( const uint8_t primaryid, const uint8_t subid, AkmECompass::DeviceId* devid);

private:
    Ticker				ticker;
    AkmECompass*		compass;
    AkmECompass::Mode	mode;
    uint8_t				numOptions;
};

#endif
