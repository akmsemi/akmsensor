#ifndef AK7401CTRL_H
#define AK7401CTRL_H

#include "mbed.h"
#include "akmsensor.h"
#include "ak7401.h"
//#include "akdp_debug.h"

/**
 * Class for handling commands issued to the AK7401.
 */
class Ak7401Ctrl : public AkmSensor
{

public:
    
    typedef AkmSensor base;

    /**
     * Device Sub-ID.
     */
    typedef enum {
        SUB_ID_AK7401              = 0x02,          /**< AK7401: ID = 02h */
    } SubIdAngleSensor;

    /**
     * Constructor.
     *
     */
    Ak7401Ctrl();

    /**
     * Destructor.
     *
     */
    virtual ~Ak7401Ctrl();
    
    /**
     * Process for intializing the selected sensor.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status init(const uint8_t id, const uint8_t subid);
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor();
    
    /**
     * Process abstraction for starting sensor operation.
     *
     * @param sec Number of seconds of operation.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status startSensor(const float sec);
    
    /**
     * Process abstraction for stopping sensor operation.
     *
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status stopSensor();
    
    /**
     * Process abstraction for reading data from the sensor.
     *
     * @param msg Message object that will hold the sensor data.
     * @return Termination status type for debugging purposes.
     */
    virtual AkmSensor::Status readSensorData(Message* msg);
    
    /**
     * Primary process for interfacing a sensor with the AKDP.  When implemented
     * in sensor class, it will transfer commands between the the sensor control
     * class and AkmSensorManager. 
     *
     * @param in Command message to be processed by sensor.
     * @param out Message returned from sensor.
     * @return Termination status type for debugging purposes.
     */
    virtual Status requestCommand(Message* in, Message* out);

private:   
    Ticker          ticker;
    AK7401*         ak7401;
    float           interval;
};

#endif
